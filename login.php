<?php
    require 'config.php';
        
    $redirect_after_login = 'index.php';

    $remember_password = strtotime('+'. (string)$session_length . 'days');
    
    if(isset($_POST['password']))
    {
        $pwdhash = hash('sha256', $_POST['password']);
    }
    
    if (isset($_POST['password']) && $pwdhash == $password) {
        setcookie("password", $password, $remember_password);
        header('Location: ' . $redirect_after_login);
        exit;
    }
    else if (isset($_POST['password'])){
        header('Location: login.php?incorrect');
    }
?>
<!DOCTYPE html>
<html>
<head>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>kokosnote 🥥 login</title>
    <link rel="stylesheet" href="style.css">
    <style>
        .li_coco
        {
            display:block;
            width:100%;
            top:50%;
            margin-top:-300px;
            position:absolute;
            text-align:center;
            font-size:300px;
            opacity:20%;
        }
        
        .li_box
        {
            display:block;
            width:350px;
            height:120px;
            background:#a2ae66;
            top:50%;
            left:50%;
            margin-top:-100px;
            margin-left:-175px;
            position:absolute;
            text-align:center;
            border-radius:20px;
            padding-top:50px;
        }
    </style>
</head>
<body>
<div class="n_bg">
<div class="li_coco">
🥥
</div>
    <div class="li_box">
        WELCOME 🥥 LOG IN TO CONTINUE<br><br>
        <form method="POST">
            <input name="password" type="password" autofocus>
        </form>
        <?php
        if (isset($_GET['incorrect']))
        {
            echo ('incorrect password');
        }
        
        ?>
    </div>
</div>
</body>
</html>
