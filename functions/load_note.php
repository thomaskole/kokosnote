<?php
    require_once('../protect.php');
    $filepath = "../data/".$_GET['note'].'.md';
    $file_response['note'] = file_get_contents ($filepath);
    $file_response['filetime'] = filemtime($filepath);
    if(!file_exists($filepath))
    {
        http_response_code(404);
        die();
    }
    echo json_encode($file_response);
?>
