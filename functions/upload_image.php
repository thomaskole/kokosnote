<?php

    require_once('../protect.php');
    $notepath = $_COOKIE['notepath'].'/';
    $dirname = '../data/'.$notepath;
    
    if (!file_exists($dirname)) {
        mkdir($dirname, 0777, true);
    }

    $filename = basename($_FILES['image']['name']);
    $extension_pos = strrpos($filename, '.'); // find position of the last dot, so where the extension starts
    $uploadfile = substr($filename, 0, $extension_pos) . '_' . time() . substr($filename, $extension_pos);

    if (move_uploaded_file($_FILES['image']['tmp_name'], $dirname.$uploadfile)) {
        $json_response['data']['filePath'] = 'functions/load_img.php?img='.urlencode($notepath.$uploadfile);
    } else {
        $json_response['error']['something went wrong uploading the image to '.$dirname] = $uploadfile;
    }

    echo json_encode($json_response)

?>
