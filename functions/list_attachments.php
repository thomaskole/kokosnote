<?php
    require_once('../protect.php');
    
    $attachments = glob('../data/'.$_GET['note']."/*");
    
    $returnjson = array();
    
    clearstatcache();
    
    foreach($attachments as $a)
    {
        $attachment['filepath'] = $a;
        $attachment['filename'] = basename($a);
        $attachment['filesize'] = number_format(filesize($a) / 1048576, 2) . ' MB';
        $attachment['filetime'] = date ("F d Y", filemtime($a));
        array_push($returnjson, $attachment);
    }
    
    
    
    echo (json_encode($returnjson));
    
?>
