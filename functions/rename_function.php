<?php
 require_once('../protect.php');
 function renameNote($old_cat, $new_cat, $old_note, $new_note)
 {
    
    $old_cat_path = "../data/".$old_cat."/";
    $new_cat_path = "../data/".$new_cat."/";
    $oldpath = $old_cat_path.$old_note;
    $newpath= $new_cat_path.$new_note;
    
    if(file_exists($newpath.'.md'))
    {
        $json_response['error'] = $newpath." already exists!";
        echo json_encode($json_response);
        exit;
    }
    if(!rename($oldpath.'.md', $newpath.'.md'))
    {
        $json_response['error'] = "something went wrong while renaming note.\nOld name: ".$oldpath."\nNew name: ".$newpath;
        echo json_encode($json_response);
        exit;
    }
    
    if(file_exists($oldpath))
    {
        #there's images!
        
        rename($oldpath, $newpath);
        $file_contents = file_get_contents($newpath.".md");
        
        $to_find    = "load_img.php?img=".urlencode($cat."/".$old_note);
        $to_replace = "load_img.php?img=".urlencode($cat."/".$new_note);
        
        $file_contents = str_replace($to_find,$to_replace,$file_contents);
        
        file_put_contents($newpath.".md",$file_contents);
    
    }
        
    $json_response['success'] = "Successfully renamed\n".$oldpath."\nto\n".$newpath;
    return($json_response);
 }
?>
