<?php
    require_once('../protect.php');
    $to_delete = $_GET['note'];
    unlink('../data/'.$to_delete.'.md');    
    
    #there might me images left in this note - delete them!
    $dir_to_delete = '../data/'.$to_delete.'/';
    if (file_exists($dir_to_delete )) {
        array_map('unlink', glob("$dir_to_delete*.*"));
        rmdir($dir_to_delete );
    }
?>
