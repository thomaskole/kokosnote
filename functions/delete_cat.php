<?php
    require_once('../protect.php');

function delete_directory($dirname) {
    if (is_dir($dirname))
        $dir_handle = opendir($dirname);
    if (!$dir_handle)
          return false;
    while($file = readdir($dir_handle)) {
           if ($file != "." && $file != "..") {
                if (!is_dir($dirname."/".$file))
                     unlink($dirname."/".$file);
                else
                     delete_directory($dirname.'/'.$file);
           }
     }
     closedir($dir_handle);
     rmdir($dirname);
     return true;
}   

    $dirname = '../data/'.$_GET['cat'].'/';
    if(delete_directory($dirname))
    {   $rsp['success'] = 'Category removed';
        echo(json_encode($rsp));
    }
    else
    {   $rsp['error'] = "Something went wrong removing category: \n".$dirname;
        echo(json_encode($rsp));
    }
?>
