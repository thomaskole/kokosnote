<?php
    require_once('../protect.php');
    require '../config.php';
    $notes = glob('../data/'.$_GET['cat'].'/*.md');
    if($note_sort_order != 'alphabetical')
        array_multisort(array_map('filemtime', $notes), SORT_NUMERIC, SORT_DESC, $notes);
    echo (json_encode($notes));
?>
