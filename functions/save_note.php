<?php
    require_once('../protect.php');
    /* PUT data comes in on the stdin stream */
    $putdata = fopen("php://input", "r");
    
    /* Open a file for writing */
    $filepath = "../data/".$_GET['note'].'.md';
    $filetime_current = filemtime($filepath);
    $filetime_received = $_GET['filetime'];
    
    $json_response['hello'] = 'hello';
    
    $dirname = dirname($filepath);
    if (!is_dir($dirname))
    {
        mkdir($dirname, 0755, true);
        $json_response['overwritten'] = "looks like someone has removed the entire category this note belonged to.\nSaved anyway.";
    }
    else if(!file_exists($filepath))
    {
        $json_response['overwritten'] = "looks like someone deleted the note in the meantime.\nSaved anyway.";
    }
    else if(strcmp($filetime_current,$filetime_received)!= 0)
    {
        #someone else already saved this file before this request.        
        $newpath = substr($filepath, 0, -3);
        $newpath = $newpath . '_' . filemtime($filepath) . '.md';
        rename($filepath, $newpath);
        $json_response['overwritten'] = "looks like someone already saved the file before you!\n" . "your note has been saved, the previous version has been backed up to\n" . $newpath;
    }
    
    $fp = fopen($filepath, "w");

    /* Read the data 1 KB at a time
    and write to the file */
    while ($data = fread($putdata, 1024))
    fwrite($fp, $data);

    /* Close the streams */
    fclose($fp);
    fclose($putdata);
    clearstatcache();
        
    $json_response['filetime'] = filemtime($filepath);
    $json_response['prev_filetime'] = $filetime_current;
    $json_response['prev_send'] = $filetime_received;
    echo json_encode($json_response);
?>
