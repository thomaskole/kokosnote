<?php
    require_once('protect.php');
?>

<!doctype html>
<html>
<head>
<title>kokosnote 🥥</title>
<link rel="stylesheet" href="EasyMDE/easymde.min.css">
<script src="EasyMDE/easymde.min.js"></script>
<link rel="stylesheet" href="style.css">
<script src="functions/sanitize-filename.js"></script>
<script src="functions.js"></script>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
</head>
<body>

<div class="n_bg"></div>
<div class="n_bg_dither"></div>

<div class="n_container">
<!--categories-->
    <div class="n_body" id="n_body">
        <div class="n_body_header">
            <a id="n_breadcrumb" class="miniheader" style="text-align:left">select a note to edit...</a>
        </div>
        <div class="n_body_footer">
            <button onClick="n_attachments_show()"><a class="fa fa-paperclip"></a><a id="n_attachment_count">(1)</a></button>
            <button onClick="n_discard()">discard</button>
            <button onClick="save_note()">save</button>
        </div>
        <textarea id="my-text-area"></textarea>
        <script>
        var easyMDE = new EasyMDE({element: document.getElementById('my-text-area'), toolbar: ["preview", "bold", "italic", "strikethrough", "heading", "code", "unordered-list", "link"], uploadImage: true, imageUploadEndpoint: 'functions/upload_image.php', imageMaxSize: (1024*1024*6)});
        easyMDE.togglePreview();
        </script>
    </div>
    <div class="n_logout">
        <a href="logout.php">log out</a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a href="functions/takeout_self.php" target="_blank">download as .zip</a>
    </div>
    <div class="n_sidebar foldaway" id="n_sidebar">
        <div class="n_list_panel">
            <div class="n_list_header n_list_header_round"><center>🥥 kokosnote</center></div>
            <div class="n_list_body n_list_categories">
                <ul id="n_categories" class="n_items"></ul> 
            </div>
            <div class="n_list_buttons n_list_categories n_list_buttons_round"><button onClick="rename_cat()" title="rename selected category"><a class="fa fa-pencil-square-o"></a></button><button onClick="remove_cat()">−</button><button onClick="create_category()">+</button></div>
        </div>
        <div class="n_list_panel">
            <div class="n_list_header"></div>
            <div class="n_list_body n_list_notes">
                <ul id="n_notes" class="n_items"></ul> 
            </div>
            <div class="n_list_buttons n_list_notes"><button onClick="rename_note()" title="rename current note"><a class="fa fa-pencil-square-o"></a></button><button onClick="delete_note()" title="delecte current note">−</button><button onClick="create_note()" title="add a new note">+</button></div>
        </div>
    </div>
    <div class="toaster"><div class="toast_wrapper" id="toaster"></div></div>
    <button class="n_toggle_sidebar_button fa-bars fa n_button" onClick="toggleSidebar()"></button>
	<div id="n_attachments_blackout" class="n_attachments_blackout" style="display:none;"></div>
    <div class="n_attachments_container" id="n_attachments_container" style="display:none;">

		<div class="n_list_header n_attachments_header" id="n_attachments_header">Attachments for <button onClick="n_attachments_hide()">close</button><button onClick="n_upload_attachment()">upload</button><input id="fileinput" type="file"></input></div>
		<div class="n_list_body n_attachment_body" id="n_attachments_body">
		</div>

	<div>
    
</div>



<script>

function set_last_loaded_cookie()
{
    document.cookie = "notepath=" + v_note_path + "; SameSite=Strict";     
}

var path_cookie = readCookie("notepath");
refresh_categories();

if(path_cookie)
{
    v_note_path = path_cookie;   
    select_category(v_note_path.split('/')[0]);    
    load_note();
}

</script>
</body>
</html>
