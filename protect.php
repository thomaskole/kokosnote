<?php
    require 'config.php';

    if (empty($_COOKIE['password']) || $_COOKIE['password'] !== $password) {
        header('Location: ./login.php');
        exit;
    }
    if (isset($_POST['password']) && $pwdhash == $password) {
        setcookie("password", $password, $remember_password);
    }
?>
